import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class Red extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text></Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      width: 100,
      height: 100,
      backgroundColor: 'red',
    },
  });
