import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Header from './Header'
import Middle from './Middle'
import Body from './Body'

const Gallery = () => {
    return (
        <View style={styles.container}>
        <Header />
        <Middle />
        <Body />
        </View>
    )
}

export default Gallery

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      }
})
