import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import CompoImage from './CompoImage'

const Body = () => {
    return (
        <View style={styles.container}>
           <View style={styles.column1}>
          <CompoImage imageSource={require('../assets/img3.jpeg')} />
        </View>
        <View style={styles.column2}>
          <CompoImage imageSource={require('../assets/img4.jpg')} />
        </View>
        <View style={styles.full}>
          <CompoImage imageSource={require('../assets/img5.jpeg')} />
        </View>

        <View style={styles.column2}>
          <CompoImage imageSource={require('../assets/img6.jpeg')} />
        </View>
        <View style={styles.column1}>
          <CompoImage imageSource={require('../assets/img2.jpeg')} />
        </View>
        <View style={styles.full}>
          <CompoImage imageSource={require('../assets/img3.jpeg')} />
        </View>


        <View style={styles.column1}>
          <CompoImage imageSource={require('../assets/img6.jpeg')} />
        </View>
        <View style={styles.column2}>
          <CompoImage imageSource={require('../assets/img2.jpeg')} />
        </View>
        <View style={styles.full}>
          <CompoImage imageSource={require('../assets/img3.jpeg')} />
        </View>
        </View>
    )
}

export default Body

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding: 5,
        backgroundColor: '#3b5998',
      },
      column1: {
        flex: 1,
        padding: 5,
      },
      column2: {
        flex: 3,
        padding: 5,
      },
      full: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
      },
})
