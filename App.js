import React from 'react';
import { StyleSheet, Text, View,StatusBar} from 'react-native';



export default function App() {
  return (
    <View style={{
      flex: 1,
      backgroundColor: '#000',
    }}>
      <StatusBar backgroundColor="blue" barStyle="light-content" showHideTransition='slide' />
      <Text style={{color:'#fff'}}>HEllo</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
